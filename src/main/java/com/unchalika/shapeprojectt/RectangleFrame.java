/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.shapeprojectt;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Tuf Gaming
 */
public class RectangleFrame {

    public static void main(String[] args) {
        JFrame frame = new JFrame("Rectangle");
        frame.setSize(350, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblW = new JLabel("wide:", JLabel.TRAILING);
        lblW.setSize(50, 20);
        lblW.setLocation(5, 5);
        lblW.setBackground(Color.WHITE);
        lblW.setOpaque(true);
        frame.add(lblW);

        JLabel lblL = new JLabel("long:", JLabel.TRAILING);
        lblL.setSize(50, 20);
        lblL.setLocation(5, 25);
        lblL.setBackground(Color.WHITE);
        lblL.setOpaque(true);
        frame.add(lblL);

        final JTextField txtW = new JTextField();
        txtW.setSize(50, 20);
        txtW.setLocation(60, 5);
        frame.add(txtW);

        final JTextField txtL = new JTextField();
        txtL.setSize(50, 20);
        txtL.setLocation(60, 27);
        frame.add(txtL);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Rectangle wide= ??? long= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
//Even Driven
        btnCalculate.addActionListener(new ActionListener() {//Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    String strw = txtW.getText();
                    String strl = txtL.getText();
                    double w = Double.parseDouble(strw);
                    double l = Double.parseDouble(strl);
                    Rectangle rectangle = new Rectangle(w, l);

                    lblResult.setText("Rectangle w = " + String.format("%.2f", rectangle.getW()) + "Rectangle l =" + String.format("%.2f", rectangle.getL())
                            + "area = " + String.format("%.2f", rectangle.calArea())
                            + "peimeter = " + String.format("%.2f", rectangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error:Pleas input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtW.setText("");
                    txtW.requestFocus();

                    txtL.setText("");
                    txtL.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
