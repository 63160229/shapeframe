/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.shapeprojectt;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
/**
 *
 * @author Tuf Gaming
 */
public class SquareFrame2 extends JFrame {
   JLabel lblSide;
    JTextField txtSide;
    JButton btnCalculate;
    JLabel lblResult;
    public SquareFrame2() {
        super("Square");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);
        
        lblSide = new JLabel("side:", JLabel.TRAILING);
        lblSide.setSize(50, 20);
        lblSide.setLocation(5, 5);
        lblSide.setBackground(Color.WHITE);
        lblSide.setOpaque(true);
        this.add(lblSide);

        txtSide = new JTextField();
        txtSide.setSize(50, 20);
        txtSide.setLocation(60, 5);
        this.add(txtSide);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Square side= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(300, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);
//Even Driven
        btnCalculate.addActionListener(new ActionListener() {//Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                   
                    String strSide = txtSide.getText();
                    
                    double side = Double.parseDouble(strSide);
                    
                    Square square = new Square(side);
                   
                    lblResult.setText("Square d = " + String.format("%.2f", square.getSide())
                            + "area = " + String.format("%.2f", square.calArea())
                            + "perimeter = " + String.format("%.2f", square.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(SquareFrame2.this, "Error:Pleas input number",
                             "Error", JOptionPane.ERROR_MESSAGE);
                    txtSide.setText("");
                    txtSide.requestFocus();
                }
            }
        });
    }
   
 public static void main(String[] args) {
         SquareFrame2 frame = new SquareFrame2();
        frame.setVisible(true);
    }
}