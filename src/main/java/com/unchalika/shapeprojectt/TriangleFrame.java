/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.shapeprojectt;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Tuf Gaming
 */
public class TriangleFrame {
    public static void main(String[] args) {
        JFrame frame = new JFrame("Triangle");
        frame.setSize(350, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLayout(null);

        JLabel lblB = new JLabel("base:", JLabel.TRAILING);
        lblB.setSize(50, 20);
        lblB.setLocation(5, 5);
        lblB.setBackground(Color.WHITE);
        lblB.setOpaque(true);
        frame.add(lblB);

        JLabel lblH = new JLabel("hight:", JLabel.TRAILING);
        lblH.setSize(50, 20);
        lblH.setLocation(5, 25);
        lblH.setBackground(Color.WHITE);
        lblH.setOpaque(true);
        frame.add(lblH);

        final JTextField txtB = new JTextField();
        txtB.setSize(50, 20);
        txtB.setLocation(60, 5);
        frame.add(txtB);

        final JTextField txtH = new JTextField();
        txtH.setSize(50, 20);
        txtH.setLocation(60, 27);
        frame.add(txtH);

        JButton btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        frame.add(btnCalculate);

        final JLabel lblResult = new JLabel("Triangle base= ??? hight= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        frame.add(lblResult);
        
//Even Driven
        btnCalculate.addActionListener(new ActionListener() {//Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    String strw = txtB.getText();
                    String strl = txtH.getText();
                    double b = Double.parseDouble(strw);
                    double h = Double.parseDouble(strl);
                    Triangle triangle= new Triangle(b, h);

                    lblResult.setText("Triangle b = " + String.format("%.2f", triangle.getB()) + "Triangle h =" + String.format("%.2f", triangle.getH())
                            + "area = " + String.format("%.2f", triangle.calArea())
                            + "peimeter = " + String.format("%.2f", triangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(frame, "Error:Pleas input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtB.setText("");
                    txtB.requestFocus();

                    txtH.setText("");
                    txtH.requestFocus();
                }
            }
        });

        frame.setVisible(true);
    }
}
