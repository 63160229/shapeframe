/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.shapeprojectt;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

/**
 *
 * @author Tuf Gaming
 */
public class RectangleFrame2 extends JFrame {

    JLabel lblW;
    JLabel lblL;
    JTextField txtW;
    JTextField txtL;
    JButton btnCalculate;
    JLabel lblResult;

    public RectangleFrame2() {
        super("Rectangle");
        this.setSize(300, 300);
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        this.setLayout(null);

        lblW = new JLabel("wide:", JLabel.TRAILING);
        lblW.setSize(50, 20);
        lblW.setLocation(5, 5);
        lblW.setBackground(Color.WHITE);
        lblW.setOpaque(true);
        this.add(lblW);

        lblL = new JLabel("long:", JLabel.TRAILING);
        lblL.setSize(50, 20);
        lblL.setLocation(5, 25);
        lblL.setBackground(Color.WHITE);
        lblL.setOpaque(true);
        this.add(lblL);

        txtW = new JTextField();
        txtW.setSize(50, 20);
        txtW.setLocation(60, 5);
        this.add(txtW);

        txtL = new JTextField();
        txtL.setSize(50, 20);
        txtL.setLocation(60, 27);
        this.add(txtL);

        btnCalculate = new JButton("Calculate");
        btnCalculate.setSize(100, 20);
        btnCalculate.setLocation(120, 5);
        this.add(btnCalculate);

        lblResult = new JLabel("Rectangle wide= ??? long= ??? area= ??? perimeter= ???");
        lblResult.setHorizontalAlignment(JLabel.CENTER);
        lblResult.setSize(350, 50);
        lblResult.setLocation(0, 50);
        lblResult.setBackground(Color.MAGENTA);
        lblResult.setOpaque(true);
        this.add(lblResult);

//Even Driven
        btnCalculate.addActionListener(new ActionListener() {//Anonymous
            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    String strw = txtW.getText();
                    String strl = txtL.getText();
                    double w = Double.parseDouble(strw);
                    double l = Double.parseDouble(strl);
                    Rectangle rectangle = new Rectangle(w, l);

                    lblResult.setText("Rectangle w = " + String.format("%.2f", rectangle.getW()) + "Rectangle l =" + String.format("%.2f", rectangle.getL())
                            + "area = " + String.format("%.2f", rectangle.calArea())
                            + "peimeter = " + String.format("%.2f", rectangle.calPerimeter()));

                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(RectangleFrame2.this, "Error:Pleas input number",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    txtW.setText("");
                    txtW.requestFocus();

                    txtL.setText("");
                    txtL.requestFocus();
                }
            }
        });
    }

    public static void main(String[] args) {
        RectangleFrame2 frame = new RectangleFrame2();
        frame.setVisible(true);
    }
}
