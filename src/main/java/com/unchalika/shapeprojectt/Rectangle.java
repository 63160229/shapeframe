/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.unchalika.shapeprojectt;

/**
 *
 * @author Tuf Gaming
 */
public class Rectangle extends Shape {

    private double w;
    private double l ;

    public Rectangle(double w,double l) {
        super("Rectangle");
        this.w = w;
        this.l = l;
    }

   
 

    public double getW() {
        return w;
    }

    public void setW(double w) {
        this.w = w;
    }

    public double getL() {
        return l;
    }

    public void setL(double l) {
        this.l = l;
    }


    @Override
    public double calArea() {
        return w*l;
    }

    @Override
    public double calPerimeter() {
        return 2*(w+l);
    }
}
